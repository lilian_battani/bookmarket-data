package net.tncy.bookmarket.data;

public class Book {

    public enum BookFormat{
        BROCHE,
        POCHE
    }

    private int id;
    private String title;
    private String author;
    private String publisher;
    private Book.BookFormat format;
    private String isbn;

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getPublisher() {
        return publisher;
    }

    public BookFormat getFormat() {
        return format;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public void setFormat(BookFormat format) {
        this.format = format;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }
}
