package net.tncy.bookmarket.data;

import java.util.ArrayList;
import java.util.List;

public class BookStore {

    private int id;
    private String name;
    private List<InventoryEntry> inventoryEntries;

    public BookStore() {
        this.inventoryEntries = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<InventoryEntry> getInventoryEntries() {
        return inventoryEntries;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }
}
